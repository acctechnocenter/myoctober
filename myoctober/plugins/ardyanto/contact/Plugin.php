<?php namespace Ardyanto\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
     public function registerComponents()
    {
        return [
            'Ardyanto\Contact\Components\ContactForm' => 'contactform',
        ];
    }


    public function registerSettings()
    {
    }
}
