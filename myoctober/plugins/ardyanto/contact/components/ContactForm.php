<?php
namespace Ardyanto\Contact\Components;

use Cms\Classes\ComponentBase;

class ContactForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Contact Form',
            'description' => 'IMC Contact Form'
        ];
    }

}