<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoDokterStaff4 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_dokter_staff', function($table)
        {
            $table->dropColumn('spesialis_name');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_dokter_staff', function($table)
        {
            $table->string('spesialis_name', 191);
        });
    }
}
