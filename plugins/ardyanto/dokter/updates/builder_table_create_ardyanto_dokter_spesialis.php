<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoDokterSpesialis extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_dokter_spesialis', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('spesialis_name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_dokter_spesialis');
    }
}
