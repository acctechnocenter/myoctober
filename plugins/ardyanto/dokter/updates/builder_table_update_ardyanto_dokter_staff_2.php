<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoDokterStaff2 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_dokter_staff', function($table)
        {
            $table->string('doctor_image');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_dokter_staff', function($table)
        {
            $table->dropColumn('doctor_image');
        });
    }
}
