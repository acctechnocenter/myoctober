<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoDokterSpesialis extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_dokter_spesialis', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('spesialis_name')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_dokter_spesialis', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->increments('id')->unsigned()->change();
            $table->string('spesialis_name', 191)->change();
        });
    }
}
