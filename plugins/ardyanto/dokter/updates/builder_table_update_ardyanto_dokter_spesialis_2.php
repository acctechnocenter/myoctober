<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoDokterSpesialis2 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_dokter_spesialis', function($table)
        {
            $table->boolean('status');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_dokter_spesialis', function($table)
        {
            $table->dropColumn('status');
        });
    }
}
