<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoDokterSchedule extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_dokter_schedule', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('doctor_id');
            $table->string('day');
            $table->text('time');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_dokter_schedule');
    }
}
