<?php namespace Ardyanto\Dokter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoDokterStaff extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_dokter_staff', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_spesialis');
            $table->string('spesialis_name');
            $table->text('doctor_description');
            $table->boolean('doctor_status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_dokter_staff');
    }
}
