<?php namespace Ardyanto\Dokter\Models;

use Url;
use Model;
use Ardyanto\Dokter\Models\DoctorSpesialis;
use Ardyanto\Dokter\Models\DoctorSchedule;

/**
 * Model
 */
class DoctorStaff extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_dokter_staff';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $jsonable = ['doctor_schedule'];


    /* Relations */
    public $attachOne = [
        'doctor_image' => 'System\Models\File'
    ];

    public $hasOne = [
        'DoctorSpesialis' => ['Ardyanto\Dokter\Models\DoctorSpesialis', 'key' => 'id', 'otherKey' => 'id_spesialis']
    ];

    public $hasMany = [
        'DoctorSchedule' => ['Ardyanto\Dokter\Models\DoctorSchedule', 'key' => 'doctor_id', 'otherKey' => 'id']
    ];

    public function getIdSpesialisOptions()
    {
        return DoctorSpesialis::all()->lists('spesialis_name', 'id');
    }

    /**
     * Generate a URL slug for this model
     */
    public function afterSave()
    {
        $deleteSchedule = DoctorSchedule::where('doctor_id', $this->id)->delete();
        if(!empty($this->doctor_schedule)){
            foreach ($this->doctor_schedule as $key => $value) {
                $schedule = new DoctorSchedule;
                $schedule->doctor_id = $this->id;
                $schedule->day = !empty($value['hari']) ? $value['hari'] : '';
                $schedule->time = !empty($value['jam']) ? $value['jam'] : '';
                $schedule->save();
            }
        }
        
    }

    /**
     * Sets the "url" attribute with a URL to this object
     * @param string $pageName
     * @param Cms\Classes\Controller $controller
     */
    public function setUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->slug,
        ];

        return $this->url = $controller->pageUrl($pageName, $params);
    }

     /**
     * Lists posts for the front end
     *
     * @param        $query
     * @param  array $options Display options
     *
     * @return Post
     */
    public function scopeListFrontEnd($query, $options)
    {
        extract(array_merge([
            'page'       => 1,
            'perPage'    => 30,
            'spesialisFilter'  => null,
            'doctorNameFilter' => null,
            'dayFilter' => null,
        ], $options));

        if($spesialisFilter){
            $query->where('id_spesialis', '=', $spesialisFilter);
        }

        if($doctorNameFilter){
            $query->where('doctor_name', 'like', '%' . $doctorNameFilter . '%');
        }
        
        if($dayFilter){
            $query->whereHas('DoctorSchedule', function($q) use ($dayFilter) {
                $q->where('day', $dayFilter);
            });
        }

        if($doctorNameFilter && $spesialisFilter){
            $query->where('doctor_name', 'like', '%' . $doctorNameFilter . '%')->where('id_spesialis', '=', $spesialisFilter);
        }
        return $query->paginate($perPage, $page);
    }
}
