<?php namespace Ardyanto\Dokter\Models;

use Model;

/**
 * Model
 */
class DoctorSchedule extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    //use \October\Rain\Database\Traits\SoftDelete;

    //protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_dokter_schedule';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
