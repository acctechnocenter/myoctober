<?php namespace Ardyanto\Dokter\Components;

use Redirect;
use BackendAuth;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Input;
use Ardyanto\Dokter\Models\DoctorStaff;
use Ardyanto\Dokter\Models\DoctorSpesialis;

class FilterDoctor extends ComponentBase
{

    
    public $doctor;
    public $spesialis;
    public $doctorNames;
    public $spesialisSelected;
    public $nameDoctor;
    public $daySelected;
    public $days;

     /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;
    /**
     * Reference to the page name for linking to doctors.
     * @var string
     */
    public $postPage;

    public function componentDetails(){
        return [
            'name' => 'Filter Doctor',
            'description' => 'Filter Doctor'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title'       => 'rainlab.blog::lang.settings.posts_pagination',
                'description' => 'rainlab.blog::lang.settings.posts_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title'             => 'rainlab.blog::lang.settings.posts_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'rainlab.blog::lang.settings.posts_per_page_validation',
                'default'           => '10',
            ]
        ];
    }

    public function onRun() {
        $this->prepareVars();
        $this->doctor = $this->filterDoctor();
        $this->spesialis = DoctorSpesialis::all()->lists('spesialis_name', 'id');
        $this->spesialisSelected = !empty(Input::get('spesialis')) ? Input::get('spesialis') : '';
        $this->nameDoctor = !empty(Input::get('doctorname')) ? Input::get('doctorname') : '';
        $this->daySelected = !empty(Input::get('day')) ? Input::get('day') : '';
        $this->days = [
                        'senin' => 'Senin',
                        'selasa' => 'Selasa',
                        'rabu' => 'Rabu',
                        'kamis' => 'Kamis',
                        'jumat' => 'Jumat',
                        'sabtu' => 'Sabtu',
                        'minggu' => 'Minggu'
                    ];

         /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');
            if ($currentPage > ($lastPage = $this->doctor->lastPage()) && $currentPage > 1)
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
        }
        //$this->doctorNames = $this->filterByNames();
    }

    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->postPage = $this->page['postPage'] = $this->property('postPage');
    }

    public function filterByNames() {
        $doctorStaff = DoctorStaff::all();
        $doctorNames = [];

        foreach($query as $doctorStaff){
            $doctorNames[] = $doctorStaff->doctor_name;
        }

        $doctorNames = $doctorNames;//array_unique($years);
        return $doctorNames;
    }

    protected function filterDoctor() {
        $spesialis = Input::get('spesialis');
        $doctorname = Input::get('doctorname');
        $day = Input::get('day');
        //$query = DoctorStaff::get();
        $doctors = DoctorStaff::with('DoctorSpesialis')->listFrontEnd([
            'page'             => $this->property('pageNumber'),
            'perPage'          => $this->property('postsPerPage'),
            'spesialisFilter'  => $spesialis,
            'doctorNameFilter' => $doctorname,
            'dayFilter'       => $day,
        ]);

                /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $doctors->each(function($post) {
            $post->setUrl($this->postPage, $this->controller);
        });

        return $doctors;
    }

}