<?php namespace Ardyanto\Dokter;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ardyanto\Dokter\Components\FilterDoctor' => 'filterdoctor'
        ];
    }

    public function registerSettings()
    {
    }
}
