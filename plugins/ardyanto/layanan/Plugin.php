<?php namespace Ardyanto\Layanan;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ardyanto\Layanan\Components\Layanan' => 'layanan',
            'Ardyanto\Layanan\Components\LayananHome' => 'layananhome'
        ];
    }

    public function registerSettings()
    {
    }
}
