<?php namespace Ardyanto\Layanan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoLayananLayanan extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_layanan_layanan', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title_layanan');
            $table->string('slug');
            $table->string('description');
            $table->string('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_layanan_layanan');
    }
}
