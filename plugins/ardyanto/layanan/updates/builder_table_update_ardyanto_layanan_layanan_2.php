<?php namespace Ardyanto\Layanan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoLayananLayanan2 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_layanan_layanan', function($table)
        {
            $table->integer('flag_home');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_layanan_layanan', function($table)
        {
            $table->dropColumn('flag_home');
        });
    }
}
