<?php namespace Ardyanto\Layanan\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoLayananLayanan extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_layanan_layanan', function($table)
        {
            $table->dropColumn('sort_order');
            $table->dropColumn('nest_left');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_layanan_layanan', function($table)
        {
            $table->integer('sort_order')->default(0);
            $table->integer('nest_left')->default(0);
        });
    }
}
