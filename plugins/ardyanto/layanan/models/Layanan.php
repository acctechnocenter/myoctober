<?php namespace Ardyanto\Layanan\Models;

use Model;

/**
 * Model
 */
class Layanan extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_layanan_layanan';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

       /* Relations */
    public $attachOne = [
        'layanan_image' => 'System\Models\File'
    ];
}
