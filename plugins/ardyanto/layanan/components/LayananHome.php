<?php namespace Ardyanto\Layanan\Components;

use Redirect;
use Cms\Classes\ComponentBase;
use Ardyanto\Layanan\Models\Layanan as ModelLayananHome;

class LayananHome extends ComponentBase
{

    public $services;

    public function componentDetails(){
        return [
            'name' => 'LayananHome',
            'description' => 'List LayananHome IMC'
        ];
    }

    public function onRun() {
        $this->services = ModelLayananHome::where('status', 'active')->where('flag_home', '1')->orderBy('created_at', 'desc')->get();
    }




}