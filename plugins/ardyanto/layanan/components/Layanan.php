<?php namespace Ardyanto\Layanan\Components;

use Redirect;
use Cms\Classes\ComponentBase;
use Ardyanto\Layanan\Models\Layanan as ModelLayanan;

class Layanan extends ComponentBase
{

    public $services;

    public function componentDetails(){
        return [
            'name' => 'Layanan',
            'description' => 'List Layanan IMC'
        ];
    }

    public function onRun() {
        $this->services = ModelLayanan::where('status', 'active')->orderBy('created_at', 'desc')->get();
    }




}