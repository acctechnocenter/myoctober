<?php namespace Ardyanto\Karir\Models;

use Model;

/**
 * Model
 */
class Karir extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_karir_karir';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $attachOne = [
        'karir_image' => 'System\Models\File'
    ];
    
}
