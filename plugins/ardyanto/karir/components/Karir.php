<?php namespace Ardyanto\Karir\Components;

use Redirect;
use Cms\Classes\ComponentBase;
use Ardyanto\Karir\Models\Karir as ModelKarir;

class Karir extends ComponentBase
{

    public $careers;

    public function componentDetails(){
        return [
            'name' => 'Karir',
            'description' => 'List Karir IMC'
        ];
    }

    public function onRun() {
        $this->careers = ModelKarir::where('status', 'active')->orderBy('created_at', 'desc')->get();
    }




}