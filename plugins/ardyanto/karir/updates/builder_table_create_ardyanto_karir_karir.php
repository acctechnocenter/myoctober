<?php namespace Ardyanto\Karir\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoKarirKarir extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_karir_karir', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title_career');
            $table->string('slug');
            $table->text('description');
            $table->boolean('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_karir_karir');
    }
}
