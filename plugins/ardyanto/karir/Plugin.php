<?php namespace Ardyanto\Karir;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ardyanto\Karir\Components\Karir' => 'karir'
        ];
    }

    public function registerSettings()
    {
    }
}
