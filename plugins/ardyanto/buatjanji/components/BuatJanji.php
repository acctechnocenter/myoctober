<?php namespace Ardyanto\BuatJanji\Components;

use Input;
use Validator;
use Redirect;
use Flash;
use Cms\Classes\ComponentBase;
use Ardyanto\Dokter\Models\DoctorSpesialis;
use Ardyanto\Dokter\Models\DoctorStaff;
use Ardyanto\BuatJanji\Models\BuatJanji as ModelBuatJanji;

class BuatJanji extends ComponentBase
{

    public $spesialis;
    public $doctors;

    public function componentDetails(){
        return [
            'name' => 'Buat janji Form',
            'description' => 'Enter Buat janji'
        ];
    }

    public function onRun() {
        $this->spesialis = DoctorSpesialis::all()->lists('spesialis_name', 'id');
        $this->doctors = DoctorStaff::all()->lists('doctor_name', 'id');
    }


    public function onSave(){
       $buatjanji = new ModelBuatJanji();
       $buatjanji->id_spesialis = Input::get('input_spesialis');
       if(!empty($buatjanji->id_spesialis)){
           $nameSpesialis = DoctorSpesialis::find($buatjanji->id_spesialis);
       }
       $buatjanji->spesialis_name = !empty($nameSpesialis->spesialis_name) ? $nameSpesialis->spesialis_name : '';
       $buatjanji->doctor_id =Input::get('input_dokter');
       if(!empty($buatjanji->doctor_id)){
            $doctorName = DoctorStaff::find($buatjanji->doctor_id);
       }
       $buatjanji->doctor_name = !empty($doctorName->doctor_name) ? $doctorName->doctor_name : '';
       $buatjanji->appointment_date = Input::get('input_date');
       $buatjanji->patient_name = Input::get('input_fullname');
       $buatjanji->no_handphone = Input::get('input_telephone');
       $buatjanji->email = Input::get('input_email');
       $buatjanji->message = Input::get('input_message');
       $buatjanji->save();
       Flash::success('Terima Kasih data saudara kami simpan. Team administrasi kami akan segera menghubungi anda.');
       return Redirect::back();
    }

}