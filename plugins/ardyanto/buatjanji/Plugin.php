<?php namespace Ardyanto\BuatJanji;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ardyanto\BuatJanji\Components\BuatJanji' => 'buatjanji'
        ];
    }

    public function registerSettings()
    {
    }
}
