<?php namespace Ardyanto\BuatJanji\Models;

use Model;


/**
 * Model
 */
class BuatJanji extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_buatjanji_buatjanji';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasOne = [
        'DoctorSpesialis' => ['Ardyanto\Dokter\Models\DoctorSpesialis', 'key' => 'id', 'otherKey' => 'id_spesialis']
    ];


}
