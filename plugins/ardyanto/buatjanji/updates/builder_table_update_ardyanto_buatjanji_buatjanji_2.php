<?php namespace Ardyanto\BuatJanji\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoBuatjanjiBuatjanji2 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->string('spesialis_name', 191)->nullable()->change();
            $table->integer('doctor_id')->nullable()->change();
            $table->string('doctor_name', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->string('spesialis_name', 191)->nullable(false)->change();
            $table->integer('doctor_id')->nullable(false)->change();
            $table->string('doctor_name', 191)->nullable(false)->change();
        });
    }
}
