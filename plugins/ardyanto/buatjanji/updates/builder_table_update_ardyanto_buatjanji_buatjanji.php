<?php namespace Ardyanto\BuatJanji\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoBuatjanjiBuatjanji extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->text('message');
            $table->boolean('status');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->dropColumn('message');
            $table->dropColumn('status');
        });
    }
}
