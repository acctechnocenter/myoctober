<?php namespace Ardyanto\BuatJanji\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoBuatjanjiBuatjanji3 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->string('patient_name');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->dropColumn('patient_name');
        });
    }
}
