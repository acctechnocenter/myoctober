<?php namespace Ardyanto\BuatJanji\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoBuatjanjiBuatjanji extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_buatjanji_buatjanji', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_spesialis');
            $table->string('spesialis_name');
            $table->integer('doctor_id');
            $table->string('doctor_name');
            $table->date('appointment_date');
            $table->string('no_handphone');
            $table->string('email');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_buatjanji_buatjanji');
    }
}
