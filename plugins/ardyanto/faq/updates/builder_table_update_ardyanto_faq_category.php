<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqCategory extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_category', function($table)
        {
            $table->string('is_published');
            $table->increments('id')->unsigned(false)->change();
            $table->string('category_name')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_category', function($table)
        {
            $table->dropColumn('is_published');
            $table->increments('id')->unsigned()->change();
            $table->string('category_name', 191)->change();
        });
    }
}
