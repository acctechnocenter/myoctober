<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateArdyantoFaqFaqs extends Migration
{
    public function up()
    {
        Schema::create('ardyanto_faq_faqs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('question');
            $table->text('answer');
            $table->boolean('published');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ardyanto_faq_faqs');
    }
}
