<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqCategory2 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_category', function($table)
        {
            $table->string('category_name')->change();
            $table->string('is_published')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_category', function($table)
        {
            $table->string('category_name', 191)->change();
            $table->string('is_published', 191)->change();
        });
    }
}
