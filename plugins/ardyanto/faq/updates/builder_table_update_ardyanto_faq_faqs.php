<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqFaqs extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('published')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->boolean('published')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
