<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqFaqs3 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->string('gender');
            $table->string('name_pasien');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->dropColumn('gender');
            $table->dropColumn('name_pasien');
        });
    }
}
