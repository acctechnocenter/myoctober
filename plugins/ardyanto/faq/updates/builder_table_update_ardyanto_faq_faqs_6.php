<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqFaqs6 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
