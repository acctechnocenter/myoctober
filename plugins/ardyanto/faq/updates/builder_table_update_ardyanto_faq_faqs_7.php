<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqFaqs7 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->string('email_pasien');
            $table->string('no_handphone_pasien');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->dropColumn('email_pasien');
            $table->dropColumn('no_handphone_pasien');
        });
    }
}
