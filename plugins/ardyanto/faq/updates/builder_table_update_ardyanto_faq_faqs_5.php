<?php namespace Ardyanto\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateArdyantoFaqFaqs5 extends Migration
{
    public function up()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->text('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ardyanto_faq_faqs', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
