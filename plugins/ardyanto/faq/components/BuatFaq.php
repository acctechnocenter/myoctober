<?php namespace Ardyanto\Faq\Components;

use Input;
use Validator;
use URL;
use Redirect;
use Flash;
use Str;
use Cms\Classes\ComponentBase;
use Ardyanto\Dokter\Models\DoctorSpesialis;
use Ardyanto\Faq\Models\Faqs as ModelBuatFaqs;

class BuatFaq extends ComponentBase
{

    public $spesialis;
    public $showtopic;

    public function componentDetails(){
        return [
            'name' => 'Buat Faq Form',
            'description' => 'Enter Buat Faq'
        ];
    }

    public function onRun() {
        $this->spesialis = DoctorSpesialis::all()->lists('spesialis_name', 'id');
        $this->showtopic = !empty(Input::get('showtopic')) ? Input::get('showtopic') : '';
    }

    public function onSave(){
        $buatfaq = new ModelBuatFaqs();
        $buatfaq->category_id = Input::get('input_category_id');
        $buatfaq->name_pasien = Input::get('input_name_pasien');
        $buatfaq->email_pasien = Input::get('input_email_pasien');
        $buatfaq->no_handphone_pasien = Input::get('input_no_handphone_pasien');
        $buatfaq->slug = Str::slug(Input::get('input_question_title'));
        $buatfaq->question_title = Input::get('input_question_title');
        $buatfaq->question = Input::get('input_question');
        $buatfaq->published = 'not_active';
        $buatfaq->save();
        Flash::success('Terima kasih pertanyaan saudara kami simpan. Team dokter kami akan segera menjawab pertanyaan anda.');

        return Redirect::to("/tanya-dokter?showtopic=success");
    
        //return Redirect::to($url);
        //return Redirect::back();
     }

}