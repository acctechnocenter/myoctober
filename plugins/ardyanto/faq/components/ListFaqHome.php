<?php namespace Ardyanto\Faq\Components;

use Redirect;
use Cms\Classes\ComponentBase;
use Ardyanto\Faq\Models\Faqs as ModelFaqs;

class ListFaqHome extends ComponentBase
{

    public $faqs;

    public function componentDetails(){
        return [
            'name' => 'List FAQ Home',
            'description' => 'List Faq Home IMC'
        ];
    }

    public function onRun() {
        $this->faqs = ModelFaqs::where('published', 'active')->orderBy('created_at', 'desc')->get();
    }




}