<?php namespace Ardyanto\Faq\Components;

use Redirect;
use Cms\Classes\ComponentBase;
use Ardyanto\Faq\Models\Faqs as ModelFaqs;

class ListFaq extends ComponentBase
{

    public $faqs;

    public function componentDetails(){
        return [
            'name' => 'List FAQ',
            'description' => 'List Faq IMC'
        ];
    }

    public function onRun() {
        $this->faqs = ModelFaqs::where('published', 'active')->orderBy('created_at', 'desc')->get();
    }




}