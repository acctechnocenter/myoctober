<?php namespace Ardyanto\Faq\Models;

use Model;

/**
 * Model
 */
class FaqsCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_faq_category';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
