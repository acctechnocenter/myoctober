<?php namespace Ardyanto\Faq\Models;

use Model;
use Ardyanto\Faq\Models\FaqsCategory;
use Ardyanto\Dokter\Models\DoctorSpesialis;

/**
 * Model
 */
class Faqs extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ardyanto_faq_faqs';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasOne = [
        'FaqsCategory' => ['Ardyanto\Faq\Models\FaqsCategory', 'key' => 'id', 'otherKey' => 'category_id'],
        'DoctorSpesialis' => ['Ardyanto\Dokter\Models\DoctorSpesialis', 'key' => 'id', 'otherKey' => 'category_id']
    ];

    /* Relations */
    public $attachOne = [
        'faq_image' => 'System\Models\File',
        'faq_doctor_image' => 'System\Models\File',
    ];

    public function getCategoryIdOptions()
    {
        return DoctorSpesialis::all()->lists('spesialis_name', 'id');
    }

    /**
     * Scope a query to only include active users.
     */
    public function scopeActive($query)
    {
        return $query->where('published', 'active');
    }

}
