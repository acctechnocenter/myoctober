<?php namespace Ardyanto\Faq;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ardyanto\Faq\Components\ListFaqHome' => 'listfaqhome',
            'Ardyanto\Faq\Components\ListFaq' => 'listfaq',
            'Ardyanto\Faq\Components\BuatFaq' => 'buatfaq',
        ];
    }

    public function registerSettings()
    {
    }
}
